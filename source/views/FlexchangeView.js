import React, { Component, StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  Text,
} from 'react-native';
import { connect } from 'react-redux';
import ExchangeView from './ExchangeView';
import PureListView from '../components/PureListView';
import FlexchangeAgenda from '../components/FlexchangeAgenda';
import ExchangeListItem from '../components/ExchangeListItem';

class FlexchangeView extends Component {
  constructor() {
    super();

    this.state = {
      day: null,
    };
  }

  handleAgendaDayPress(day) {
    this.setState({ day });
  }

  handleExchangePress(exchange) {
    const { navigator } = this.props;

    navigator.push({
      component: ExchangeView,
      passProps: { exchange }
    })
  }

  render() {
    const { day } = this.state;
    const { flexchange, weeks } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.tabs}>
          <TouchableOpacity style={[styles.tab, styles.borderRight]}>
            <Text>Beschikbare shifts</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tab}>
            <Text>Mijn Flexchange</Text>
          </TouchableOpacity>
        </View>

        <ScrollView style={styles.container}>
          <View style={styles.agenda}>
            <FlexchangeAgenda
              currentWeeks={flexchange.currentWeeks}
              openDay={this.handleAgendaDayPress.bind(this)}/>
          </View>

          {(() => {
            if (day) {
              return (
                <View style={styles.container}>
                  <Text style={styles.date}>{day.date.format('dddd DD MMMM')}</Text>

                  <PureListView
                    data={[
                      {
                        title: 'Test1',
                      }, {
                        title: 'Test2',
                      }
                    ]}
                    renderRow={row => (
                      <ExchangeListItem
                        item={row}
                        onPress={this.handleExchangePress.bind(this, row)}/>
                    )}/>
                </View>
              );
            }
          })()}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabs: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    marginBottom: 15,
  },
  tab: {
    flex: 1,
    height: 44,
    justifyContent: 'center',
    alignItems: 'center',
  },
  borderRight: {
    borderRightWidth: StyleSheet.hairlineWidth,
    borderRightColor: '#eee',
  },
  date: {
    textAlign: 'center',
    fontWeight: 'bold',
    marginVertical: 15,
  }
});

function mapStateToProps(state) {
  return { flexchange: state.flexchange, weeks: state.weeks };
}

export default connect(mapStateToProps)(FlexchangeView);
