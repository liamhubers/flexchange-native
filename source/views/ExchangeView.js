import React, { Component, StyleSheet,
  View,
  Text
} from 'react-native';

class ExchangeView extends Component {
  render() {
    const { exchange } = this.props;

    return (
      <View style={styles.container}>
        <Text>{exchange.title}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
})

export default ExchangeView;
