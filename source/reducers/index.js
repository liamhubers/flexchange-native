import { combineReducers } from 'redux';

import flexchange from './flexchange';
import weeks from './weeks';

export default combineReducers({
  flexchange,
  weeks,
})
