import moment from 'moment';

const today = moment();

const initialState = {
  currentWeeks: [today.isoWeek(), today.add(1, 'weeks').isoWeek()]
};

export default function flexchange(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}
