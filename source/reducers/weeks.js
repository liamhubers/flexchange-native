const initialState = {
  items: [
    {
      week: 18,
      exchanges: [
        {
          title: 'Test1',
        }, {
          title: 'Test2',
        }
      ],
    }, {
      week: 19,
      exchanges: [
        {
          title: 'Test3',
        }, {
          title: 'Test4',
        }
      ]
    }
  ],
};

export default function weeks(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}
