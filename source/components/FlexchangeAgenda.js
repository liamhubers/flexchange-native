import React, { Component, StyleSheet,
  View,
  TouchableOpacity,
  Text,
} from 'react-native';
import FlexchangeAgendaDay from './FlexchangeAgendaDay';

import moment from 'moment';

class FlexchangeAgenda extends Component {
  render() {
    const { currentWeeks } = this.props;

    return (
      <View style={[styles.container, styles.agenda]}>
        <Text style={styles.title}>Mei - Juni</Text>

        <View style={styles.week}>
          {(() => {
            return ['MA', 'DI', 'WO', 'DO', 'VR', 'ZA', 'ZO'].map(day => {
              return <Text style={[styles.dayText, styles.dayName]} key={day}>{day}</Text>;
            })
          })()}
        </View>

        {(() => {
          return currentWeeks.map(week => {
            return this.renderWeek(week);
          })
        })()}
      </View>
    )
  }

  renderWeek(week) {
    const startOfWeek = moment().isoWeek(week).startOf('isoweek');

    return (
      <View style={styles.week} key={week}>
        {(() => {
          return [0,1,2,3,4,5,6].map((day, index) => {
            return (
              <FlexchangeAgendaDay
                day={{
                  date: moment(startOfWeek).add(day, 'days'),
                  exchanges: [],
                }}
                onPress={this.props.openDay}
                key={index}/>
            );
          });
        })()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  agenda: {
    backgroundColor: '#fff',
    padding: 15,
  },
  title: {
    marginBottom: 10,
    textAlign: 'center',
    fontSize: 18,
  },
  week: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 5,
  },
  dayName: {
    flex: 1,
    color: '#ccc',
    textAlign: 'center',
  }
});

export default FlexchangeAgenda;
