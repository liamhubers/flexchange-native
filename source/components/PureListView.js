import React, { Component,
  ListView
} from 'react-native';

class PureListView extends Component {
  constructor(props) {
    super(props);

    const dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2,
    });

    this.state = {
      dataSource: dataSource.cloneWithRows(props.data),
    };
  }

  componentWillReceiveProps(nextProps: Props) {
    if (this.props.data !== nextProps.data) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(nextProps.data),
      });
    }
  }

  render() {
    const { dataSource } = this.state;

    return <ListView dataSource={dataSource} {...this.props}/>
  }
}

export default PureListView;
