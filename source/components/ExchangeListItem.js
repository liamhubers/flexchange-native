import React, { StyleSheet,
  TouchableOpacity,
  Text
} from 'react-native';

const ExchangeListItem = ({ item, onPress }) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Text style={styles.title}>{item.title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
    backgroundColor: '#fff',
    marginBottom: 2,
  },
  title: {
    color: 'red',
  },
});

export default ExchangeListItem;
