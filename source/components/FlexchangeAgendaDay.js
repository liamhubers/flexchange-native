import React, { Component, StyleSheet,
  TouchableOpacity,
  Text
} from 'react-native';

const FlexchangeAgendaDay = ({ day, onPress }) => {
  const style = [styles.day];
  if (day.exchanges.length) style.push(styles.active);

  const textStyle = day.exchanges.length ? styles.activeText : {};

  return (
    <TouchableOpacity style={style} onPress={onPress.bind(null, day)}>
      <Text style={textStyle}>{day.date.date()}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  day: {
    flex: 1,
    height: 44,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 2,
    borderRadius: 22,
  },
  active: {
    backgroundColor: '#9ae0c3',
  },
  activeText: {
    color: '#fff',
  }
})

export default FlexchangeAgendaDay
