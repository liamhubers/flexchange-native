import React, { AppRegistry, Component, StyleSheet,
  View,
  StatusBar,
  NavigatorIOS
} from 'react-native';
import { Provider } from 'react-redux';
import store from './source/configureStore';
import FlexchangeView from './source/views/FlexchangeView';

// Initialize moment locale for further usage
import moment from 'moment';
import 'moment/locale/nl';
moment.locale('nl');

class flexchange extends Component {
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <StatusBar
            backgroundColor="#5BA6E6"
            barStyle="light-content"
          />
          <NavigatorIOS
            initialRoute={{
              component: FlexchangeView,
              title: 'Flexchange',
            }}
            style={styles.container}
            shadowHidden={true}
            translucent={false}
            barTintColor="#5ba6e6"
            titleTextColor="#fff"
            tintColor="#fff"
            itemWrapperStyle={{
              backgroundColor: '#f7f7f7'
            }}
          />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

AppRegistry.registerComponent('flexchange', () => flexchange);
